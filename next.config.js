/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    SOURCE_URL: process.env.SOURCE_URL
  }
}

module.exports = nextConfig
