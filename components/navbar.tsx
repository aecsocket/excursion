import styles from "./Navbar.module.css";
import Link from "next/link";

const Navbar = () => {
    return (
        <nav className={styles.navbar}>
            <div className={styles.header}>
                <div className={styles.main}>
                    <Link href="/"><a>
                        <svg
                            width="32"
                            height="32"
                            viewBox="0 0 64 64"
                            xmlns="http://www.w3.org/2000/svg"
                            className={`${styles.item} ${styles.btn} ${styles.icon}`}
                        >
                            <path d="M 32,1.566191 41.888543,21.602569 64,24.815553 48,40.411698 51.777086,62.433809 32,52.036378 12.222911,62.433807 16,40.411698 0,24.815551 16,24 42,48 24,18 Z"/>
                        </svg>
                    </a></Link>
                </div>

                <div className={styles.user}>
                    <Link href="/login"><a className={`${styles.item} ${styles.btn} ${styles.btnText} ${styles.em}`}>
                        <div>
                            Log In
                        </div>
                    </a></Link>
                    <Link href="/register"><a className={`${styles.item} ${styles.btn} ${styles.btnText}`}>
                        <div>
                            Register
                        </div>
                    </a></Link>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
