import styles from "./Footer.module.css";
import Link from "next/link";

const Footer = () => {
    return (
        <footer className={styles.footer}>
            <div className={styles.content}>
                <ul className={styles.column}>
                    <li className={styles.item}>
                        <Link href="/docs"><a className={styles.link}>
                            API
                        </a></Link>
                    </li>
                </ul>
                <ul className={styles.column}>
                    <li className={styles.item}>
                        <a href={process.env.SOURCE_URL ?? "/404"} className={styles.link}>
                            Source
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    );
};

export default Footer;
