import styles from "../styles/Index.module.css";
import Head from "next/head";
import Link from "next/link";
import { Component } from "react";

export default class Index extends Component {
    render() {
        const sourceUrl = process.env.SOURCE_URL;
        return (
            <div className={styles.container}>
                <Head>
                    <title>Excursion</title>
                </Head>

                <div className={styles.jumbotron}>
                    <div className={styles.content}>
                        <h1 className={styles.title}>The financial dashboard for all</h1>
                        <h2 className={styles.caption}>A convenient, open-source webapp for stocks and crypto.</h2>
                        <Link href="/register"><a>
                            <button className={`${styles.btn} ${styles.accentBtn}`}>
                                Register your account
                            </button>
                        </a></Link>
                    </div>
                </div>

                <div className={styles.sections}>
                    <div className={styles.content}>
                        <div className={styles.section}>
                            <h1 className={styles.title}>
                                About
                            </h1>
                            <p className={styles.desc}>
                                Excursion is a free and open-source website, designed from the ground up
                                to allow users to track stock and cryptocurrency prices without hassle.
                            </p>
                            <p className={styles.caption}>
                            Trade in the future, not the past.
                            </p>
                        </div>

                        <div className={styles.section}>
                            <h1 className={styles.title}>
                                Technologies
                            </h1>
                            <p className={styles.desc}>
                                This is a hobby project that I started to experiment with web development.
                                This uses:
                            </p>
                            <ul className={styles.list}>
                                <li><a className={styles.link} href="https://nextjs.org/">Next.js</a> for rendering the site</li>
                                <li><a className={styles.link} href="https://www.postgresql.org/">PostgreSQL</a> as the backend database</li>
                                <li><a className={styles.link} href="https://www.prisma.io/">Prisma</a> as the database ORM</li>
                                <li><a className={styles.link} href="https://fontawesome.com/">Font Awesome</a> for the icon sets</li>
                            </ul>
                            {
                                sourceUrl ?
                                    <a href={sourceUrl}>
                                        <button className={`${styles.btn}`}>
                                            View the source
                                        </button>
                                    </a> : ""
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
