import "../styles/globals.css";
import "@fortawesome/fontawesome-svg-core";
import { config } from "@fortawesome/fontawesome-svg-core";
import App from "next/app";
import Layout from "../components/layout";

config.autoAddCss = false;

export default class ExcursionApp extends App {
    state = {
        user: null
    };

    render() {
        const { Component, pageProps } = this.props;
        return (
            <Layout>
                <Component {...pageProps}/>
            </Layout>
        );
    }
}
